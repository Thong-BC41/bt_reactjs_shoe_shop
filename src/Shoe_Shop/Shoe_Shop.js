import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { data_shoe } from "./Data_Shoe";
import ListShoe from "./ListShoe";

export default class Shoe_Shop extends Component {
  state = {
    listShoe: data_shoe,
    cart: [],
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soluong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soluong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleChangeQuantity = (idShoe, tanggiam) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == idShoe;
    });
    cloneCart[index].soluong = cloneCart[index].soluong + tanggiam;
    this.setState({
      cart: cloneCart,
    });
  };

  handleDelete = (idShoe) => {
    let newCart = this.state.cart.filter((item) => {
      return item.id != idShoe;
    });
    this.setState({ cart: newCart });
  };

  render() {
    return (
      <div class="container">
        <h2>Shoe_Shop</h2>
        <div>
          <div>
            <CartShoe
              cart={this.state.cart}
              handleDelete={this.handleDelete}
              handleChangeQuantity={this.handleChangeQuantity}
            />
          </div>
          <div>
            <ListShoe
              list={this.state.listShoe}
              handleAdd={this.handleAddToCart}
            />
          </div>
        </div>
      </div>
    );
  }
}
